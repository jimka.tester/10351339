# SEO Technical Task 

To run the tests from command prompt

mvn clean

mvn clean verify

To run VisaConfirmation scenarios

mvn clean test -Dcucumber.options="src/test/resources/features/VisaConfirmation.feature"

To run MetOfficeAPI scenarios

mvn clean test -Dcucumber.options="src/test/resources/features/MetOfficeAPI.feature"

once the build is successful copy the url provided
for the reports and paste in browser to see the results report.




