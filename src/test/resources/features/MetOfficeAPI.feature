@API @Test
Feature: Verify Met Office Site List API Endpoints

  Background:
    Given I have a valid Met Office API key

  Scenario: Retrieve and Verify Site List and get location ID
    When I make a request to the Met Office 'sitelist' endpoint
    Then the API response should contain the site list
    And the response status code should be 200
    And I get the location ID for location "Croydon"

  Scenario: Retrieve location and Verify Daily Forecast Data and Verify parameter S has windspeed
    When I make a request to the Met Office 'sitelist' endpoint
    Then the API response should contain the site list
    And the response status code should be 200
    When I make a request to the Met Office daily forecast data endpoint for "Croydon"
    Then the API response should contain the daily forecast data
    And the response status code should be 200
    And Verify the parameter S has description windspeed in API response

  Scenario: Retrieve and Verify Three-Hourly Forecast Data
    When I make a request to the Met Office three-hourly forecast data endpoint
    Then the API response should contain the three-hourly forecast data
    And the response status code should be 200
