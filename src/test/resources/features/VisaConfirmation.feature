@UI @Test
Feature: Visa checks

  Background:
    Given I am on the Check UK visa website
    And I start visa check

  Scenario: 01 - An Australian coming to the UK for Tourism.
    When I select a nationality of 'Australia'
    And I select reason 'Tourism'
    Then I will be informed 'You will not need a visa to come to the UK'

  Scenario: 02 - A Chilean coming to the UK for Work and plans on staying for longer than 6 months.
    When I select a nationality of 'Chile'
    And I select reason 'Work'
    And I state I am intending to stay for 'more' than 6 months
    And I select answer 'Religious' to question 'Are you planning to work in any of these types of job?'
    And I select next step button
    Then I will be informed 'You need a visa as a religious worker'

  Scenario: 03 - A Azerbaijan national coming to the UK to join a partner, who is a British citizen.
    When I select a nationality of 'Azerbaijan'
    And I select reason 'family'
    And I select answer 'Yes' to question 'Does your partner or family member have any of the following types of UK immigration status?'
    And I select next step button
    Then I will be informed 'You’ll need a visa to join your family or partner in the UK'