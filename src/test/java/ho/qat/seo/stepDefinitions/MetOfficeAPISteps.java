package ho.qat.seo.stepDefinitions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Assert;

import static io.restassured.RestAssured.given;


//import static net.serenitybdd.rest.RestRequests.*;


public class MetOfficeAPISteps {

    private String apiKey;
    private Response response;

    @Given("I have a valid Met Office API key")
    public void iHaveAValidMetOfficeAPIKey() {
        apiKey = "6d1a22e1-f704-46c3-8b2e-dadb11d2834e";
        RestAssured.baseURI = "http://datapoint.metoffice.gov.uk/";
    }

    @When("I make a request to the Met Office 'sitelist' endpoint")
    public void iMakeARequestToTheMetOfficeSiteListEndpoint() {
        response = given().
                when().
                get("public/data/val/wxfcs/all/json/sitelist?key=" + apiKey);
        System.out.println(response);
    }

    @When("I make a request to the Met Office daily forecast data endpoint for {string}")
    public void iMakeARequestToTheMetOfficeDailyForecastDataEndpointFor(String location) {
        JsonNode jsonResponse = parseJsonResponse(response.getBody().asString());
        String locationId = getLocationIdByName(jsonResponse, location);
        Assert.assertNotNull("Location ID not found for ", location);
        response = given()
                .when()
                .get("/public/data/val/wxfcs/all/json/" + locationId + "?res=daily&key=" + apiKey);
    }

    @And("Verify the parameter S has description windspeed in API response")
    public void verifyTheParamterSHasDescriptionWindspeedInAPIResponse() throws JsonProcessingException {
        JsonNode jsonResponse = parseJsonResponse(response.getBody().asString());

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(String.valueOf(jsonResponse));

        JsonNode paramNode = jsonNode
                .path("SiteRep")
                .path("Wx")
                .path("Param");

        // Find the parameter with name 'S' and check its description
        String actualDescription = null;
        for (JsonNode param : paramNode) {
            if ("S".equals(param.path("name").asText())) {
                actualDescription = param.path("$").asText();
                break;
            }
        }
        System.out.println("Actual Description for 'S': " + actualDescription);
        Assert.assertEquals("Expected Description for 'S' does not match actual value.", "Wind Speed", actualDescription);
    }

    @When("I make a request to the Met Office three-hourly forecast data endpoint")
    public void iMakeARequestToTheMetOfficeThreeHourlyForecastEndpoint() {
        response = given()
                .when()
                .get("/public/data/val/wxfcs/all/json/324152?res=3hourly&key=" + apiKey);
        System.out.println(response);
    }

    @Then("the API response should contain the site list")
    public void theAPIResponseShouldContainTheSiteList() {
        System.out.println("API Response: " + response.getBody().asString());
        Assert.assertTrue(response.getBody().asString().contains("region"));
    }

    @Then("the API response should contain the daily forecast data")
    public void theAPIResponseShouldContainTheDailyForecastData() {
        System.out.println("API Response: " + response.getBody().asString());
        Assert.assertTrue(response.getBody().asString().trim().contains("SiteRep"));
    }

    @Then("the API response should contain the three-hourly forecast data")
    public void theAPIResponseShouldContainTheThreeHourlyForecastData() {
        System.out.println("API Response: " + response.getBody().asString());
        Assert.assertTrue(response.getBody().asString().contains("SiteRep"));
    }

    @Then("the response status code should be {int}")
    public void theResponseStatusCodeShouldBe(int expectedStatusCode) {
        Assert.assertEquals(expectedStatusCode, response.getStatusCode());
    }

    @And("I get the location ID for location {string}")
    public void IGetTheLocationIDForLocation(String location){

        JsonNode jsonResponse = parseJsonResponse(response.getBody().asString());

        String locationId = getLocationIdByName(jsonResponse, location);

        Assert.assertNotNull("Location ID not found for ", location);
        System.out.println("Location ID : " + locationId);

    }

    private JsonNode parseJsonResponse(String jsonResponse) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readTree(jsonResponse);
        } catch (Exception e) {
            throw new RuntimeException("Error parsing JSON response", e);
        }
    }

    private String getLocationIdByName(JsonNode jsonResponse, String locationName) {
        for (JsonNode location : jsonResponse.path("Locations").path("Location")) {
            if (location.path("name").asText().equalsIgnoreCase(locationName)) {
                return location.path("id").asText();
            }
        }
        return null;
    }

}
