package ho.qat.seo.pages;


import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;


public class CommonPage extends PageObject {

        WebElementFacade answerToSelect;
        @FindBy(css = "#current-question > button")
        private WebElementFacade nextStepButton;

        @FindBy(xpath = "//*[@id=\"current-question\"]/div/fieldset/legend/h1")
        WebElementFacade getQuestion;

        private String getExpectedQuestion(){
                return getQuestion.getText();
        }

        public void selectQandA(String question, String answer){
                Assert.assertTrue(getExpectedQuestion().equalsIgnoreCase(question));
                String selector = String.join("","input[value='", answer.toLowerCase(), "']");
                answerToSelect = find(By.cssSelector(selector));
                clickOn(answerToSelect);
        }

        public void clickNextStepButton(){
                nextStepButton.waitUntilEnabled().click();
        }
}
